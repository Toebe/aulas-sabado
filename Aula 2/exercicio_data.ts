
type intervaloDiasType = ( parameter1:number, parameter2:number ) => string


function retornaIntervalo(data1: number, data2: number){
    if (data1 > data2 || data1 === data2){
        return "A segunda data tem que ser maior que a primeira."
    }
    let diferencaMs: number = data2 - data1
    let diferencaSegundos: number = 0
    if ( diferencaMs > 1000) {
         if (diferencaMs%1000 === 0) {
             diferencaSegundos = diferencaMs / 1000
         } else  {
            console.log("Não gosto de números quebrados, irei listar a diferença em millisegundos:")
            return diferencaMs
         }
    } else {
        return "A diferenca é de menos de 1 segundo."
    }
    let diferencaMinutos: number
    if ( diferencaSegundos > 60) {
        if (diferencaSegundos%60 === 0) {
            diferencaMinutos = diferencaSegundos / 60
        } else  {
            console.log("Não gosto de números quebrados, irei listar a diferença em segundos:")
            return diferencaSegundos
        }
   } else {
       return "A diferenca é de menos de 1 minuto, irei listar a diferença em segundos:\n" + diferencaSegundos
   }
   let diferencaHoras: number
   if ( diferencaMinutos > 60) {
       if (diferencaMinutos%60 === 0) {
        diferencaHoras = diferencaMinutos / 60
        } else  {
            console.log("Não gosto de números quebrados, irei listar a diferença desta forma:")
            return "A diferença em segundos é " + diferencaSegundos 
                   + "\n"
                   + "A diferença em minutos é " + diferencaMinutos
        }
    } else {
         return "A diferenca é de menos de 1 minuto."
    }
    let diferencaDias: number
    if ( diferencaHoras > 24) {
       if (diferencaHoras%24 === 0) {
        diferencaDias = diferencaHoras / 24
        } else  {
            console.log("Não gosto de números quebrados, irei listar a diferença desta forma:")
            return "A diferença em segundos é " + diferencaSegundos 
                   + "\n"
                   + "A diferença em minutos é " + diferencaMinutos
                   + "\n"
                   + "A diferença em horas é " + diferencaHoras
        }
    } else {
        console.log("A diferença é de menos de 1 dia, irei listar a diferença desta forma:")
        return "A diferença em segundos é " + diferencaSegundos 
           + "\n"
           + "A diferença em minutos é " + diferencaMinutos
           + "\n"
           + "A diferença em horas é " + diferencaHoras
    }
    let diferencaMeses: number
    if ( diferencaDias > 30) {
       if (diferencaHoras%30 === 0) {
        diferencaMeses = diferencaDias / 30
        } else  {
            console.log("Não gosto de números quebrados, irei listar a diferença desta forma:")
            return "A diferença em segundos é " + diferencaSegundos 
               + "\n"
               + "A diferença em minutos é " + diferencaMinutos
               + "\n"
               + "A diferença em horas é " + diferencaHoras
               + "\n"
               + "A diferença em dias é " + diferencaDias
        }
    } else {
        console.log("A diferença é de menos de 1 mês, irei listar a diferença desta forma:")
        return "A diferença em segundos é " + diferencaSegundos 
               + "\n"
               + "A diferença em minutos é " + diferencaMinutos
               + "\n"
               + "A diferença em horas é " + diferencaHoras
               + "\n"
               + "A diferença em dias é " + diferencaDias
    }
    let diferencaAnos: number
    if ( diferencaMeses > 12) {
       if (diferencaDias%365 === 0) {
        diferencaAnos = diferencaMeses / 12
        } else  {
            console.log("Não gosto de números quebrados, irei listar a diferença desta forma:")
            return "A diferença em segundos é " + diferencaSegundos 
               + "\n"
               + "A diferença em minutos é " + diferencaMinutos
               + "\n"
               + "A diferença em horas é " + diferencaHoras
               + "\n"
               + "A diferença em dias é " + diferencaDias
               + "\n"
               + "A diferença em meses é " + diferencaMeses
        }
    } else {
        console.log("A diferença é de menos de 1 ano, irei listar a diferença desta forma:")
        return "A diferença em segundos é " + diferencaSegundos 
               + "\n"
               + "A diferença em minutos é " + diferencaMinutos
               + "\n"
               + "A diferença em horas é " + diferencaHoras
               + "\n"
               + "A diferença em dias é " + diferencaDias
               + "\n"
               + "A diferença em meses é " + diferencaMeses
    }
    return "A diferença em segundos é " + diferencaSegundos 
    + "\n"
    + "A diferença em minutos é " + diferencaMinutos
    + "\n"
    + "A diferença em horas é " + diferencaHoras
    + "\n"
    + "A diferença em dias é " + diferencaDias
    + "\n"
    + "A diferença em meses é " + diferencaMeses
    + "\n"
    + "A diferença em anos é " + diferencaAnos
}
console.log(retornaIntervalo(1495249200000, 1526785200000))
