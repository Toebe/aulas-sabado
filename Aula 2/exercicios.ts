type typeSoma = (vlr1: number, vlr2: number) => number

function somar(valor1: number, valor2:number){
    return valor1 + valor2
}

function subtrair(valor1: number, valor2:number){
    return valor1 - valor2
}

function multiplicar(valor1: number, valor2:number){
    return valor1 * valor2
}

function exponencializar(valor1: number, valor2:number){
    return valor1 ** valor2
}

let fun: typeSoma = somar
console.log(fun(1,2))

type typeContaCaracteresLista = (lista: Array<String>) => Array<number>

function contarCaracteresElementos (lista: Array<String>){
    for  (var strTeste of lista){
        var listaFinal = lista.map(function (valor){return valor.length})
    }
    return listaFinal
}

let fun2 = contarCaracteresElementos
console.log(fun2(["Valor","Valor2"]))

function contarCaracteresArray (lista: Array<String>){
    for  (var strTeste of lista){
        var listaFinal = lista.map(function (valor){return valor.length})
    }
    return listaFinal.reduce( ( acumulado, atual ) => {
		return acumulado + atual 
	});

}

let fun3 = contarCaracteresArray
console.log(fun3(["Valor","Valor2"]))

function calculadora(callback: typeSoma, ...valores: number[] ){
    return valores.reduce(callback)
}

console.log(calculadora(somar, 3, 3,3))
/*
console.log(calculadora(subtrair,3,3))
console.log(calculadora(multiplicar,3,3))
console.log(calculadora(exponencializar,3,3))
*/
