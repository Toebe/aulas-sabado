type TipoDigestao = (alimento: Vegetal) => Organico

interface Organico {
    decompoe(alimento: Organico) : Organico
}

interface Vegetal {
    alimenta: ( ) => void;
}

interface Inseto {
    alimentar ( alimento: Vegetal, digestao: TipoDigestao )
}

class Fezes implements Organico {
    toString() : String {
        return "fezes"
    }
    decompoe(alimento: Organico) {
        console.log("A fezes foi decomposta")
        return this
    }
}

function digerir (alimento: Planta): Organico {
    var objFezes = new Fezes ()
    console.log("Digeriu " + alimento.toString() + " e retornou " + objFezes.toString())
    return objFezes
}

class Planta implements Vegetal, Organico {
    nomePlanta: String
    constructor(nomePlanta: String){
        this.nomePlanta = nomePlanta
    }
    toString() : String {
        return this.nomePlanta
    }
    decompoe (alimento: Organico) { 
        return this
    }
    alimenta () {
        console.log("A planta recebeu os nutrientes")
    }
}

class Gafanhoto implements Inseto {
    alimentar(alimento: Vegetal, digerir: TipoDigestao) {
        console.log("O gafanhoto comeu a " + alimento.toString())
        return digerir(alimento)
    }
}

var marijuana: Planta  = new Planta("marijuana")
var objInseto: Gafanhoto = new Gafanhoto ()

objInseto.alimentar(marijuana, digerir)


/*
Tenho uma IF Organico que tem metodo alimentar e 

*/



