"use strict";
/* O que será impresso no console? */
function testeZ() {
    console.log('oi');
}
testeZ();
/* R: será impresso a string "oi" */
/* ======================================================================== */
// O que será impresso no console?
function teste2() {
    return 'teste2';
}
console.log(teste2());
//R: será impresso a string "teste2"
//========================================================================
// O que será impresso no console?
function teste3(param) {
    param + 'teste3';
    return param;
}
const variavel = 'e agora?';
console.log(teste3(variavel));
function somarValores(vlr1, vlr2) {
    return vlr1 + vlr2;
}
function subtrairValores(vlr1, vlr2) {
    return vlr1 - vlr2;
}
let funSoma = somarValores;
let funSubtrai = subtrairValores;
funSoma(1, 2);
funSubtrai(2, 2);
//========================================================================
//Escreva uma funcao que receba uma string e remova os space da string
/*
function removeEspaco (valor: String) : String {
    for (let i: 0; i > valor.length; i++){
    if (valor.indexOf(i) === ' ') {
       valor.replace(' ', '')
       i--
    }
}
}
*/
//=========================================================================
//O que será impresso no console?
function teste(list) {
    return list.filter(function (param) {
        return param > 5;
    });
}
console.log(teste([1, 2, 3, 4, 5, 6, 7, 8, 9]));
//R: 
function somar(vlr1, vlr2) {
    return vlr1 + vlr2;
}
let funAlgoComNumeros = somar;
funAlgoComNumeros(5, 7);
//R:
function retornaTamanhoString(valor) {
    return valor.length;
}
let funFazAlgoComString = retornaTamanhoString;
funFazAlgoComString("valor");
//R:
function trocaValorPorTamanho(lista) {
    lista.map(function (valor) {
        return valor.length;
    });
}
let funFazAlgoComListaString = trocaValorPorTamanho;
funFazAlgoComListaString(["valor", "valor2", "valor1o"]);
//=========================================================================
//Implemente a Interface abaixo, o nome da implementação deve LoginApi
class Session {
    constructor(token, idEmpresa) {
        this.token = token;
        this.idEmpresa = idEmpresa;
    }
}
//R:
class LoginApi {
    login(user, pass) {
        var novaSessao = new Session("a2k4l3do45", 1);
        return novaSessao;
    }
}
//=========================================================================
//Descreva com suas palavras como funciona o metodo Filter do Array.
//R:
//O método filter é utilizado em listas e irá retornar uma nova lista com todos os elementos que se passem na validação definida.
//Ex.:
var lista = ["valor", "valor2", "valor5"];
lista.filter(valor => valor.length > 3);
console.log(lista);
//irá retornar todos os elementos da lista que tenham um tamanho maior do que 5.
//["valor","valor2"]
//=========================================================================
let teste1 = (...list) => list.reduce((a, b) => a + b, 10);
console.log(teste1(1, 3, 9));
//Será retornado o valor: [[1],[3],[9]]
/*=========================================================================

Arrume o códgio para exibir a seguinte frase

Xesquedele!




let a = "Xes", b = "dele", c = "que";



function join( ...a:string[] ):string {

    let retorno = 0;

    for ( let t of a ) {

        retorno += +t;
    
    }

   return retorno;

}


console.log( join( a, c, b ) ); */
let a = "Xes", b = "dele", c = "que";
function join(...a) {
    let retorno = '';
    for (let t of a) {
        retorno += t;
    }
    return retorno;
}
//console.log( join( a, c, b ) )
