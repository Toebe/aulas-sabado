 /* O que será impresso no console? */



function testeZ():void {

    console.log('oi')

}

testeZ();

/* R: será impresso a string "oi" */


/* ======================================================================== */

// O que será impresso no console?



function teste2():string {

    return 'teste2'

}


console.log( teste2() )


//R: será impresso a string "teste2"

//========================================================================

// O que será impresso no console?



function teste3( param :string ):string {

    param + 'teste3'

    return param

}


const variavel:string = 'e agora?'

console.log( teste3( variavel ) )


//R: será impresso a String "e agora?teste3"

//========================================================================


//Escreva uma função que receba 2 numeros e retorne o resultado da equação.



//R:

type TipoEquacao = (vlr1: number, vlr2: number) => number;

function somarValores(vlr1: number, vlr2: number) : number {
	return vlr1 + vlr2
}

function subtrairValores(vlr1: number, vlr2: number) : number {
	return vlr1 - vlr2
}

let funSoma: TipoEquacao = somarValores

let funSubtrai: TipoEquacao = subtrairValores

funSoma(1,2)
funSubtrai(2,2)

//========================================================================

//Escreva uma funcao que receba uma string e remova os space da string


/*
function removeEspaco (valor: String) : String {
	for (let i: 0; i > valor.length; i++){
	if (valor.indexOf(i) === ' ') {
	   valor.replace(' ', '')
	   i--
    }
}
} 
*/
//=========================================================================

//O que será impresso no console?



function teste( list: number[] ):number[] {

    return list.filter(function (param: number) { 

        return param > 5
    
     })

}

console.log( teste( [ 1,2,3,4,5,6,7,8,9 ] ) )


//R:

//será impresso : "[ 6, 7, 8, 9 ]"

//=========================================================================

//Crie uma funcao para cada tipo definido abaixo



type FazAlgoComNumeros = ( param1: number, param2:number ) => number

//R: 

function somar (vlr1: number, vlr2: number) : number {
    return vlr1 + vlr2
}

let funAlgoComNumeros: FazAlgoComNumeros = somar
funAlgoComNumeros(5, 7)

type FazAlgoComString = ( param1: string )  => number 



//R:

function retornaTamanhoString (valor: String) : number {
      return valor.length
}

let funFazAlgoComString: FazAlgoComString = retornaTamanhoString
funFazAlgoComString("valor")


type FazAlgoComListaDeString = ( param1:string[] ) => void


//R:

function trocaValorPorTamanho( lista: Array <String> ) {
     lista.map (function (valor: String) {
         return valor.length
     })
}

let funFazAlgoComListaString: FazAlgoComListaDeString = trocaValorPorTamanho
funFazAlgoComListaString(["valor","valor2","valor1o"])

//=========================================================================

//Implemente a Interface abaixo, o nome da implementação deve LoginApi



class Session {

    token: string

    idEmpresa:number

    constructor( token: string, idEmpresa: number ) {

        this.token = token

        this.idEmpresa = idEmpresa

    }

}



interface Login {

    login( user: string, pass: string ) : Session

}


//R:

class LoginApi implements Login {
    login ( user: String, pass: String ) : Session {
	var novaSessao: Session = new Session("a2k4l3do45", 1)
	return novaSessao
    }    
}

//=========================================================================

//Descreva com suas palavras como funciona o metodo Filter do Array.



//R:

//O método filter é utilizado em listas e irá retornar uma nova lista com todos os elementos que se passem na validação definida.

//Ex.:

var lista = ["valor","valor2","valor5"]
lista.filter(valor => valor.length > 3)
console.log(lista)
//irá retornar todos os elementos da lista que tenham um tamanho maior do que 5.
//["valor","valor2"]

//=========================================================================

let teste1 = ( ...list:number[] ):number => list.reduce( ( a:number, b:number ) => a + b, 10)
console.log(teste1(1,3,9));


//Será retornado o valor: [[1],[3],[9]]


/*=========================================================================

Arrume o códgio para exibir a seguinte frase

Xesquedele!




let a = "Xes", b = "dele", c = "que";



function join( ...a:string[] ):string {

    let retorno = 0;

    for ( let t of a ) {

        retorno += +t;
    
    }

   return retorno;

}


console.log( join( a, c, b ) ); */

let a = "Xes", b = "dele", c = "que";



function join( ...a:string[] ):string {

    let retorno = '';

    for ( let t of a ) {

        retorno += t
    
    }

    return retorno;

}



//console.log( join( a, c, b ) )