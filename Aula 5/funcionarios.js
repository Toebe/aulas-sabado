var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Sexo;
(function (Sexo) {
    Sexo["MASCULINO"] = "MASCULINO";
    Sexo["FEMININO"] = "FEMININO";
})(Sexo || (Sexo = {}));
var EstadoCivil;
(function (EstadoCivil) {
    EstadoCivil["SOLTEIRO"] = "SOLTEIRO";
    EstadoCivil["CASADO"] = "CASADO";
    EstadoCivil["SEPARADO"] = "SEPARADO";
    EstadoCivil["DIVORCIADO"] = "DIVORCIADO";
    EstadoCivil["VIUVO"] = "VI\u00DAVO";
})(EstadoCivil || (EstadoCivil = {}));
/* classe funcionario
interface Funcionario extends Pessoa {
    getCargo: () => Profissao
    trabalhar: (empresaAtuacao: PessoaJuridica) => void
}*/
/* classe PJ
class PessoaJuridica implements Pessoa {
    identificador: String
    CNPJ: String
    constructor (nomeFantasia: String, CNPJ: String) {
        this.identificador = nomeFantasia
        this.CNPJ = CNPJ
    }

    getNomeFantasia () : String {
      return this.identificador
    }
    getCNPJ () : String {
        return this.CNPJ
    }
}
*/
var PessoaFisica = /** @class */ (function () {
    /*   _estadoCivil: EstadoCivil
       _sexo: Sexo */
    function PessoaFisica(nome) {
        this.identificador = nome;
    }
    /*
    setEstadoCivil (estadoCivil: EstadoCivil ) {
        this._estadoCivil = estadoCivil
    }
    getEstadOCivil () : EstadoCivil {
        return this._estadoCivil
    }

    getSexo () : Sexo {
        return this._sexo
    }*/
    PessoaFisica.prototype.getNome = function () {
        return this.identificador;
    };
    return PessoaFisica;
}());
var JogadorDeFutebol = /** @class */ (function () {
    function JogadorDeFutebol() {
    }
    JogadorDeFutebol.prototype.getAtividade = function () {
        return "JOGAR BOLA";
    };
    return JogadorDeFutebol;
}());
var Goleiro = /** @class */ (function (_super) {
    __extends(Goleiro, _super);
    function Goleiro(posicao) {
        var _this = _super.call(this) || this;
        _this._posicao = posicao;
        return _this;
    }
    Goleiro.prototype.getPosicao = function () {
        return this._posicao;
    };
    Goleiro.prototype.getAtividade = function () {
        return _super.prototype.getAtividade.call(this) + "NA POSIÇÃO DE " + this.getPosicao();
    };
    return Goleiro;
}(JogadorDeFutebol));
var Engenheiro = /** @class */ (function () {
    function Engenheiro() {
    }
    Engenheiro.prototype.getAtividade = function () {
        return "ENGENHA ";
    };
    return Engenheiro;
}());
var EngenheiroEletrico = /** @class */ (function (_super) {
    __extends(EngenheiroEletrico, _super);
    function EngenheiroEletrico(areaAtuacao) {
        var _this = _super.call(this) || this;
        _this._areaAtuacao = areaAtuacao;
        return _this;
    }
    EngenheiroEletrico.prototype.getAreaAtuacao = function () {
        return this._areaAtuacao;
    };
    EngenheiroEletrico.prototype.getAtividade = function () {
        return _super.prototype.getAtividade.call(this) + " COMO " + this.getAreaAtuacao();
    };
    return EngenheiroEletrico;
}(Engenheiro));
var Professor = /** @class */ (function () {
    function Professor() {
    }
    Professor.prototype.getAtividade = function () {
        return "MINISTRA";
    };
    return Professor;
}());
var ProfessorPortugues = /** @class */ (function (_super) {
    __extends(ProfessorPortugues, _super);
    function ProfessorPortugues(materia) {
        var _this = _super.call(this) || this;
        _this._materia = materia;
        return _this;
    }
    ProfessorPortugues.prototype.getMateria = function () {
        return this._materia;
    };
    ProfessorPortugues.prototype.getAtividade = function () {
        return _super.prototype.getAtividade.call(this) + " SOBRE " + this.getMateria();
    };
    return ProfessorPortugues;
}(Professor));
var Farejador = /** @class */ (function () {
    function Farejador() {
    }
    Farejador.prototype.getAtividade = function () {
        return "FAREJA ";
    };
    return Farejador;
}());
var CaoPolicial = /** @class */ (function (_super) {
    __extends(CaoPolicial, _super);
    function CaoPolicial(objetoFarejado) {
        var _this = _super.call(this) || this;
        _this._objetoFarejado = objetoFarejado;
        return _this;
    }
    CaoPolicial.prototype.getObjetoFarejado = function () {
        return this._objetoFarejado;
    };
    CaoPolicial.prototype.getAtividade = function () {
        return _super.prototype.getAtividade.call(this) + " " + this.getObjetoFarejado();
    };
    return CaoPolicial;
}(Farejador));
var Coordenador = /** @class */ (function () {
    function Coordenador() {
    }
    Coordenador.prototype.getAtividade = function () {
        return "COORDENA";
    };
    return Coordenador;
}());
var CoordenadorDesenvolvimento = /** @class */ (function (_super) {
    __extends(CoordenadorDesenvolvimento, _super);
    function CoordenadorDesenvolvimento(setor) {
        var _this = _super.call(this) || this;
        _this._setor = setor;
        return _this;
    }
    CoordenadorDesenvolvimento.prototype.getSetor = function () {
        return this._setor;
    };
    CoordenadorDesenvolvimento.prototype.getAtividade = function () {
        return _super.prototype.getAtividade.call(this) + " O SETOR " + this.getSetor();
    };
    return CoordenadorDesenvolvimento;
}(Coordenador));
/*
preciso construir a classe herdade e também a classe em questão

*/
var Trabalhador = /** @class */ (function () {
    function Trabalhador(nome) {
        this._nome = nome;
    }
    Trabalhador.prototype.trabalhar = function (funcao) {
        console.log("Este trabalhador " + funcao.getAtividade());
    };
    Trabalhador.prototype.getNome = function () {
        return this._nome;
    };
    return Trabalhador;
}());
var marceloGrohe = new Goleiro("Goleiro");
var engenheiroQualquer = new EngenheiroEletrico("Elétrica");
var betoven = new CaoPolicial("DROGAS");
var alma = new ProfessorPortugues("PORTUGUÊS");
var souza = new CoordenadorDesenvolvimento("DESENVOLVIMENTO");
var trabalhadorBr = new Trabalhador("trabalhador br");
console.log(trabalhadorBr.trabalhar(marceloGrohe));
console.log(trabalhadorBr.trabalhar(engenheiroQualquer));
console.log(trabalhadorBr.trabalhar(betoven));
console.log(trabalhadorBr.trabalhar(alma));
console.log(trabalhadorBr.trabalhar(souza));
