enum Sexo {
    MASCULINO = "MASCULINO",
    FEMININO = "FEMININO"
}

enum EstadoCivil {
    SOLTEIRO = "SOLTEIRO",
    CASADO = "CASADO",
    SEPARADO = "SEPARADO",
    DIVORCIADO = "DIVORCIADO",
    VIUVO = "VIÚVO"
}


interface Individuo {
    getNome: () => String
 }

interface Pessoa extends Individuo { }

interface Profissao {
    getAtividade: () => String
    /*
    salario: number
    cargo: String */
}

/* classe funcionario
interface Funcionario extends Pessoa { 
    getCargo: () => Profissao
    trabalhar: (empresaAtuacao: PessoaJuridica) => void
}*/

/* classe PJ
class PessoaJuridica implements Pessoa {
    identificador: String
    CNPJ: String
    constructor (nomeFantasia: String, CNPJ: String) {
        this.identificador = nomeFantasia
        this.CNPJ = CNPJ
    }

    getNomeFantasia () : String {
      return this.identificador  
    }
    getCNPJ () : String {
        return this.CNPJ
    }
}
*/
class PessoaFisica implements Pessoa {
    identificador: String
 /*   _estadoCivil: EstadoCivil
    _sexo: Sexo */
    
    constructor (nome: String){
        this.identificador = nome
    }
    /*
    setEstadoCivil (estadoCivil: EstadoCivil ) {
        this._estadoCivil = estadoCivil
    }
    getEstadOCivil () : EstadoCivil {
        return this._estadoCivil
    }

    getSexo () : Sexo {
        return this._sexo
    }*/

    getNome () : String {
        return this.identificador
    }

}

interface AtividadeProfissao { toString: () => String }

class JogadorDeFutebol implements Profissao {    
    getAtividade () {
        return "JOGAR BOLA"
    }
}

class Goleiro extends JogadorDeFutebol { 
    private _posicao: String
    constructor (posicao: String) {
        super()
        this._posicao = posicao
    }

    getPosicao () {
        return this._posicao
    }

    getAtividade () {
        return super.getAtividade() + "NA POSIÇÃO DE " + this.getPosicao()
    }
}

class Engenheiro implements Profissao {
    getAtividade () {
        return "ENGENHA "
    }
}

class EngenheiroEletrico extends Engenheiro {
    private _areaAtuacao: String
    constructor (areaAtuacao: String) {
        super()
        this._areaAtuacao = areaAtuacao
    }

    getAreaAtuacao () {
        return this._areaAtuacao
    }

    getAtividade () {
        return super.getAtividade() +  " COMO " + this.getAreaAtuacao()
    }
}




class Professor implements Profissao {
    getAtividade () {
        return "MINISTRA"
    }
}

class ProfessorPortugues extends Professor {
    private _materia: String
    constructor (materia: String) {
        super()
        this._materia = materia
    }

    getMateria () {
        return this._materia
    }

    getAtividade () {
        return super.getAtividade() + " SOBRE " + this.getMateria ()
    }
}

class Farejador implements Profissao {
    getAtividade () {
        return "FAREJA "
    }
}

class CaoPolicial extends Farejador {
    private _objetoFarejado: String

    constructor (objetoFarejado: String) {
        super()
        this._objetoFarejado = objetoFarejado
    }

    getObjetoFarejado () {
        return this._objetoFarejado
    }

    getAtividade () {
        return super.getAtividade() + " " + this.getObjetoFarejado()
    }
}

class Coordenador implements Profissao {
    getAtividade () {
        return "COORDENA"
    }
}

class CoordenadorDesenvolvimento extends Coordenador {
    private _setor: String

    constructor (setor: String) {
        super()
        this._setor = setor
    }

    getSetor () {
        return this._setor
    }

    getAtividade () {
        return super.getAtividade() + " O SETOR " + this.getSetor()
    }

}

    /*
preciso construir a classe herdade e também a classe em questão

*/



class Trabalhador implements Individuo {
    private _nome: String
    constructor (nome: String) {
        this._nome = nome
    }
    trabalhar (funcao: Profissao) {
        console.log( "Este trabalhador " + funcao.getAtividade() )
    }

    getNome () {
        return this._nome
    }
}

var marceloGrohe: Goleiro = new Goleiro ("Goleiro")

var engenheiroQualquer: EngenheiroEletrico = new EngenheiroEletrico ( "Elétrica" )

var betoven: CaoPolicial = new CaoPolicial ( "DROGAS" )

var alma: ProfessorPortugues = new ProfessorPortugues ( "PORTUGUÊS" )

var souza: CoordenadorDesenvolvimento = new CoordenadorDesenvolvimento ( "DESENVOLVIMENTO" )



var trabalhadorBr: Trabalhador = new Trabalhador ("trabalhador br")


console.log(trabalhadorBr.trabalhar( marceloGrohe ))
console.log(trabalhadorBr.trabalhar( engenheiroQualquer ))
console.log(trabalhadorBr.trabalhar( betoven ))
console.log(trabalhadorBr.trabalhar( alma ))
console.log(trabalhadorBr.trabalhar( souza ))