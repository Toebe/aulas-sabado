class Pessoa {
    constructor(nome, idade) {
        this._nome = nome;
        this._idade = idade;
    }
    get nome() {
        return this._nome;
    }
    get nomeVisualizacao() {
        return "Sr. " + this._nome;
    }
    set nome(nome) {
        this._nome = nome;
    }
    get idade() {
        return this._idade;
    }
    set idade(idade) {
        this._idade = idade;
    }
}
var guilherme = new Pessoa("Guilherme", 20);
var jonathan = new Pessoa("Jonathan", 18);
var renan = new Pessoa("Renan", 19);
console.log(guilherme.nome);
console.log(jonathan.nome);
console.log(renan.nome);
renan.nome = "RENANZITO";
jonathan.nome = "JOTANANNN";
guilherme.nome = "TOEBE";
console.log(guilherme.nome);
console.log(jonathan.nome);
console.log(renan.nome);
console.log(guilherme.nomeVisualizacao);
console.log(jonathan.nomeVisualizacao);
console.log(renan.nomeVisualizacao);
