/**
  * Código passado
**/

function removeItens(lista, exlusoes ) {
    for (var i =0; i<lista.length; i++ ) {
        if ( exlusoes.indexOf(lista[i] ) > -1 ) {
            lista.splice(i,1)
        }
    }
    console.log(lista)
}


/**
  * Código alterado
**/

function removeItens(lista, exlusoes ) {
    for (var i =0; i<lista.length; i++ ) {
        if ( exlusoes.indexOf(lista[i] ) > -1 ) {
            lista.splice(i,1)
            i--; /* Aqui o problema é corrigido */
        }
    }
    console.log(lista)
}