interface Organico {
    alimentar : (Organico) => void;
 }

interface Vegetal { }

interface Inseto { }

interface Anfibio { } 

interface Decompositores { }


class Planta implements Vegetal, Organico {
    alimentar(Decompositores) {
        console.log(`A planta recebeu os nutrientes.`)
    };
}

class Gafanhoto implements Inseto, Organico {x
    alimentar(Vegetal){
        console.log(`O gafanhoto comeu a planta.`)
    }
    
}

class Sapo implements Anfibio, Organico {
    alimentar(Inseto){
        console.log(`O sapo comeu o gafanhoto.`)
    }
}

class Fungos implements Decompositores, Organico {

    alimentar (Organico) {
       console.log(`Os fungos fizeram a decomposição de um orgânico.`) 
    }
}

let objVegetal: Planta = new Planta()
let objInseto: Gafanhoto = new Gafanhoto()
let objDecomp: Fungos = new Fungos()
let objAnfibio: Sapo = new Sapo()

objInseto.alimentar(objVegetal)
objAnfibio.alimentar(objInseto)
objDecomp.alimentar(objAnfibio)
objVegetal.alimentar(objDecomp)