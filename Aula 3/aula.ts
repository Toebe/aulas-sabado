enum LadoEnum {
    ESQUERDA = "ESQUERDA",
    DIREITA = "DIREITA"
}

enum Marcha {
    PONTO_MORTO,
    PRIMEIRA_MARCHA,
    SEGUNDA_MARCHA,
    TERCEIRA_MARCHA,
    QUARTA_MARCHA,
    QUINTA_MARCHA
}

interface Veiculo {
    ligar : (boolean) => void;
    acelerar : () => void;
    frear : () => void;
    dobrar : (LadoEnum) => void;
}

interface Cambio {
    reduzirMarcha : () => void;
    subirMarcha : () => void;
}

class cambioAutomatico implements Cambio{
    reduzirMarcha(){
        console.log("Marcha reduzida automaticamente.")
    }
    subirMarcha(){
        console.log("Marcha aumentada automaticamente.")
    };
}

class Carro implements Veiculo {
    cambio: Cambio;
    constructor ( cambio: Cambio ) {
        this.cambio = cambio;
    }
    ligado: boolean
    ligar( situacaoVeic: boolean ) {
        if ( this.ligado === false ) {
            this.ligado = true
        }
    }
    acelerar() {
        if (this.ligado) {
            
        }
        console.log( "VRUMMMMMMMM" )
        this.cambio.subirMarcha()
    }
    frear(){
        console.log( "RRRRRRRRRR" )
        this.cambio.reduzirMarcha()
    }
    dobrar( ladoEnum: LadoEnum ) {
        console.log( `Você dobrou a ${ladoEnum}` )
    }
}

var obj: Carro = new Carro(new cambioAutomatico())
obj.acelerar();
obj.frear();
obj.dobrar(LadoEnum.DIREITA)
obj.acelerar();